package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/config"
	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/pkg/client"
	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/pkg/types"
	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

var log *zap.SugaredLogger
var globalContext context.Context

var rootCmd = &cobra.Command{
    Use: "",
    Run: func(cmd *cobra.Command, args []string) {
        t := client.NewAppnexusClient(client.AppnexusClientConf{
            Log: log,
            Ctx: globalContext,
            AppnexusAuthorizedAcc: types.AppnexusAuthorizedAccount{
                Username: config.GetConfig().AppnexusConfig.Auth.UserName,
                Password: config.GetConfig().AppnexusConfig.Auth.Password,
            },
        })
        t.Init()
    },
}

func init() {
    prepareLogger()

    log.Info("+------------------------------------------------------+")
    log.Info("+      rtbkit-exchange-optimizer is starting !!!!      +")
    log.Info("+------------------------------------------------------+")
    log.Infof("Run program with the config: %+v", *config.GetConfig())

    signals := make(chan os.Signal, 1)
    signal.Notify(signals, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)

    ctx, cancel := context.WithCancel(context.Background())
    go func() {
        sig := <-signals
        fmt.Println("rtbkit-exchange-optimizer got signal: ", sig)
        cancel()
        os.Exit(0)
    }()
    globalContext = ctx
}

func prepareLogger() {
    conf := zap.NewDevelopmentConfig()
    level := zap.InfoLevel
    if config.GetConfig().LogLevel == "ERROR" {
        level = zap.ErrorLevel
    } else if config.GetConfig().LogLevel == "DEBUG" {
        level = zap.DebugLevel
    } else if config.GetConfig().LogLevel == "WARN" {
        level = zap.WarnLevel
    }

    conf.Level.SetLevel(level)
    logger, _ := conf.Build()
    log = logger.Sugar()
    log.Info("Log is prepared successfully")
}

// Execute command
func Execute() {
    if err := rootCmd.Execute(); err != nil {
        log.Errorf("Error while executing rootCmd: %s", err)
        os.Exit(1)
    }
}