package main

import (
	"runtime"

	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/cmd"
)

func main() {
    nCoreToRun := 1
    nCores := runtime.NumCPU()
    if nCores > 1 {
        nCoreToRun = nCores - 1
    }
    runtime.GOMAXPROCS(nCoreToRun)
    cmd.Execute()
}
