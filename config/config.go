package config

import (
	"log"

	"bitbucket.org/tinhtn1508/rtbkit-go-library/app"
	"github.com/spf13/viper"
)

// Config structure
type Config struct {
    App             app.Config
    AppConfigFile   string              `mapstructure:"appconfigfile"`
    LogLevel        string              `mapstructure:"loglevel"`
    InstanceName    string              `mapstructure:"instancename"`
    AppnexusConfig  AppnexusCredential  `mapstruture:"appnexuscredential"`
}

// AppnexusCredential ...
type AppnexusCredential struct {
    Auth  AppnexusAuthentice  `mapstructure:"auth"`
}

// AppnexusAuthentice ...
type AppnexusAuthentice struct {
    UserName       string       `mapstructure:"username"`
    Password       string       `mapstructure:"password"`
}

var values Config

func init() {
    config := viper.New()
    config.SetConfigName("exchange_optimizer_config")
    config.AddConfigPath("./config/")
    config.AutomaticEnv()
    
    if err := config.ReadInConfig(); err != nil {
        log.Fatalf("Error while reading config: %s", err)
    }

    if err := config.Unmarshal(&values); err != nil {
        log.Fatalf("Error while parsing config: %s", err)
    }

    values.App = *app.LoadAppConfig(values.AppConfigFile)
    if values.InstanceName != "" {
        values.App.GraphiteConfig.Prefix = values.InstanceName
    }
}

// GetConfig returns config instance
func GetConfig() *Config {
    return &values
}
