package utils

import (
	"math"
)

type GeoPoint struct {
    lat float64
    lon float64
}

type cellGrid struct {
    leftLeft   GeoPoint
    leftRight  GeoPoint
    rightLeft  GeoPoint
    rightRight GeoPoint
}

type limitGrid struct {
    minLat float64
    maxLat float64
    minLon float64
    maxLon float64
}

type GeoGrid struct {
    vertexPoints []GeoPoint
    distance     float64 // In km
    limit        limitGrid
    cells        []*cellGrid
}

func (grid *GeoGrid) findVertex() {
    grid.limit.minLat, grid.limit.maxLat, grid.limit.minLon, grid.limit.maxLon = math.MaxFloat64, 0.0, math.MaxFloat64, 0.0
    for _, point := range grid.vertexPoints {
        grid.limit.minLat = math.Min(grid.limit.minLat, point.lat)
        grid.limit.maxLat = math.Max(grid.limit.maxLat, point.lat)
        grid.limit.minLon = math.Min(grid.limit.minLon, point.lon)
        grid.limit.maxLon = math.Max(grid.limit.maxLon, point.lon)
    }
}

func (grid *GeoGrid) findCentroid() GeoPoint{
    num := float64(len(grid.vertexPoints))
    centroidLat := 0.0
    centroidLon := 0.0
    for _, point := range grid.vertexPoints {
        centroidLat += point.lat
        centroidLon += point.lon
    }
    return GeoPoint{centroidLat/num, centroidLon/num}
}

func (grid *GeoGrid) Build() bool {
    if len(grid.vertexPoints) < 3 {
        return false
    }
    grid.findVertex()

    checkVertexCellInPolygon := func(p GeoPoint) int {
        if grid.isInsidePolygon(p) {
            return 1
        }
        return 0
    }

    offsetLat, offsetLon := 1.0, 1.0 // use approximate method. refer to https://www.usna.edu/Users/oceano/pguth/md_help/html/approx_equivalents.htm
    for lat := grid.limit.minLat; lat < grid.limit.maxLat; lat += offsetLat {
        for lon := grid.limit.minLon; lon < grid.limit.maxLon; lon += offsetLon {
            leftRight := GeoPoint{lat, lon} // root
            leftLeft := GeoPoint{lat + offsetLat, lon}
            rightRight := GeoPoint{lat, lon + offsetLon}
            rightLeft := GeoPoint{lat + offsetLat, lon + offsetLon}

            counter := checkVertexCellInPolygon(leftRight)
            counter += checkVertexCellInPolygon(leftLeft)
            counter += checkVertexCellInPolygon(rightLeft)
            counter += checkVertexCellInPolygon(rightRight)

            if counter > 1 {
                cell := &cellGrid{
                    leftRight:  GeoPoint{lat, lon},
                    leftLeft:   GeoPoint{lat + offsetLat, lon},
                    rightLeft:  GeoPoint{lat, lon + offsetLon},
                    rightRight: GeoPoint{lat + offsetLat, lon + offsetLon},
                }
                grid.cells = append(grid.cells, cell)
            }
        }
    }
    return true
}

func (grid *GeoGrid) isInsidePolygon(p GeoPoint) bool {
    numVertex := len(grid.vertexPoints)
    counter := 0

    p1 := grid.vertexPoints[0]
    for i := 1; i <= numVertex; i++ {
        p2 := grid.vertexPoints[i%numVertex]

        if p.lat == p1.lat && p.lon == p1.lon {
            return true
        }

        if p.lat > math.Min(p1.lat, p2.lat) {
            if p.lat <= math.Max(p1.lat, p2.lat) {
                if p.lon <= math.Max(p1.lon, p2.lon) {
                    if p1.lat != p2.lat {
                        xinters := (p.lat-p1.lat)*(p2.lon-p1.lon)/(p2.lat-p1.lat) + p1.lon
                        if p1.lon == p2.lon || p.lon <= xinters {
                            counter++
                        }
                    }
                }
            }
        }
        p1 = p2
    }

    if counter%2 == 0 {
        return false
    }
    return true
}

func (grid *GeoGrid) GetCentralCell() []GeoPoint {
    var centralCells []GeoPoint
    if len(grid.cells) == 0 {
        centralCells = append(centralCells, grid.findCentroid())
        return centralCells
    }

    for _, c := range grid.cells {
        lat := c.leftRight.lat + (c.rightLeft.lat-c.leftRight.lat)/2.0
        lon := c.leftRight.lon + (c.rightLeft.lon-c.leftRight.lon)/2.0
        centralCells = append(centralCells, GeoPoint{lat, lon})
    }
    return centralCells
}

func (grid *GeoGrid) GetNumberCells() int {
	return len(grid.cells)
}
