package utils

import (
	"math"

	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/pkg/types"
	kdtree "github.com/hongshibao/go-kdtree"
)

type latlonPoint struct {
    kdtree.Point
    content     interface{}
    latlon      []float64
}

func (p latlonPoint) Dim() int {
    return 2
}

func (p latlonPoint) GetValue(dim int) float64 {
    if (dim == 0 || dim == 1) {
        return p.latlon[dim]
    }
    return 0
}

func (p latlonPoint) Distance(other kdtree.Point) float64 {
    var ret float64
    for i := 0; i < p.Dim(); i++ {
        tmp := p.GetValue(i) - other.GetValue(i)
        ret += tmp * tmp
    }
    return ret
}

func (p latlonPoint) PlaneDistance(val float64, dim int) float64 {
    tmp := p.GetValue(dim) - val
    return tmp * tmp
}

func (p *latlonPoint) getContent() interface{} {
    return p.content
}

func (p *latlonPoint) earthSurfaceDistance(other latlonPoint) float64 {
    rlat1 := p.latlon[0] * math.Pi / 180
    rlon1 := p.latlon[1] * math.Pi / 180
    rlat2 := other.GetValue(0) * math.Pi / 180
    rlon2 := other.GetValue(1) * math.Pi / 180

    h := p.__hsin(rlat2-rlat1) + math.Cos(rlat1)*math.Cos(rlat2)*p.__hsin(rlon2-rlon1)
    return 2 * types.EarthRadiusKm * math.Asin(math.Sqrt(h))
}

func (p *latlonPoint) __hsin(theta float64) float64 {
    return math.Pow(math.Sin(theta/2), 2)
}

func newLatlonPoint(content interface{}, vals []float64) latlonPoint {
    ret := latlonPoint{}
    for _, val := range vals {
        ret.latlon = append(ret.latlon, val)
    }
    ret.content = content
    return ret
}

// CoordinateKdTree ...
type CoordinateKdTree struct {
    tree            *kdtree.KDTree
    points          []kdtree.Point
}

func (tree *CoordinateKdTree) AddNewPoint(content interface{}, vals ...float64) {
    if len(vals) != 2 {
        return
    }
    tree.points = append(tree.points, newLatlonPoint(content, vals))
}

func (tree *CoordinateKdTree) FormNewKdTree() {
    if len(tree.points) <= 0 {
        return
    }
    tree.tree = kdtree.NewKDTree(tree.points)
}

func (tree *CoordinateKdTree) InRadiusKNN(radius float64, vals ...float64) []interface{} {
    var ret []interface{}

    if tree.tree == nil {
        // log.Errorf("Tree haven't been formed or failed to be formed")
        return ret
    }

    if radius < 0 || len(vals) != 2 {
        // log.Errorf("Invalid input!")
        return ret
    }

    targetPoint := newLatlonPoint(0, vals)
    knnPoints := tree.tree.KNN(targetPoint, 50)

    // log.Infof("Finding knn points, latlon = %v, radius = %f", vals, radius)
    for _, p := range knnPoints {
        pLatlon := p.(latlonPoint)
        if pLatlon.earthSurfaceDistance(targetPoint) < radius {
            ret = append(ret, pLatlon.getContent())
        } else { break }
    }

    return ret
}
