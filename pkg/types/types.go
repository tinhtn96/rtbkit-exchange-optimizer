package types

const (
    AppnexusAuthorizeAccountUrl     = "https://api.adnxs.com/auth"
    AppnexusGetBidderUrl            = "https://api.adnxs.com/bidder"
    AppnexusGetProfileUrl           = "https://api.adnxs.com/profile/%d"
    AppnexusAddProfileUrl           = "https://api.adnxs.com/profile/%d"
    AppnexusModifyProfileUrl        = "https://api.adnxs.com/profile/%d/%d"
)

type AppnexusAuthorizedAccount struct {
    Username                string                      `json:"username"`
    Password                string                      `json:"password"`
}

type AppnexusAuthorizedData struct {
    Account                 AppnexusAuthorizedAccount   `json:"auth"`
    AuthorizedToken         string
}

type AppnexusAuthorizedResponse struct {
    ResponseBody struct {
        Token               string                      `json:"token"`
        Status              string                      `json:"status"`
    }                                                   `json:"response"`
}

type BidderProfileResponse struct {
    ResponseBody struct {
        BidderProfile struct {
            Id              int64                       `json:"id"`
        }                                               `json:"bidder"`
        Status              string                      `json:"status"`
    }                                                   `json:"response"`
}

type BidderPreTargetingProfileResponse struct {
    ResponseBody struct {
        Profiles []struct {
            Id              int64                       `json:"id"`
            Description     string                      `json:"description"`
        }                                               `json:"profiles"`
        Status              string                      `json:"status"`
    }                                                   `json:"response"`
}

type GeoTargetingConfig struct {
    Id                      int64                       `json:"id"`
    Name                    string                      `json:"name"`
}

type BidderPretargetingProfile struct {
    Profile struct {
        Description         string                      `json:"description"`
        CountryAction       string                      `json:"country_action"`
        RegionAction        string                      `json:"region_action"`
        CityAction          string                      `json:"city_action"`
        CountryTargets      []GeoTargetingConfig        `json:"country_targets"`
        RegionTargets       []GeoTargetingConfig        `json:"region_targets"`
        CityTargets         []GeoTargetingConfig        `json:"city_targets"`
    }                                                   `json:"profile"`
}

type BidderAddPretargetingProfileResponse struct {
    ResponseBody struct {
        Status              string                      `json:"status"`
        Profile struct {
            Id              int64                       `json:"id"`
            CountryTargets  []GeoTargetingConfig        `json:"country_targets"`
            RegionTargets   []GeoTargetingConfig        `json:"region_targets"`
            CityTargets     []GeoTargetingConfig        `json:"city_targets"`
        }                                               `json:"profile"`
    }                                                   `json:"response"`
}

type AppnexusCityContent struct {
    countryCode                 string
    region                      string
    city                        string
    appnexusCode                int64
}

const EarthRadiusKm = 6378.1