package provider

import (
	"context"
	"encoding/csv"
	"os"
	"strconv"

	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/types"
	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/utils"
	"go.uber.org/zap"
)

// IAppnexusMapper interface
type IAppnexusMapper interface {
    AppnexusCityIDInRange(float64, float64, float64) []int64
    AppnexusCityFromPolygon([]utils.GeoPoint) []int64
    AppnexusCountryFromCode(string) int64
    AppnexusRegionFromCode(string) int64
    AppnexusCityFromCode(string) int64
}

// AppnexusMapperConf ...
type AppnexusMapperConf	struct {
    Log                 *zap.SugaredLogger
    Ctx                 context.Context
    LatlonFilename      string
    CountryFilename     string
    RegionFilename      string
    CityFilename        string
}

type appnexusMapper struct {
    log                 *zap.SugaredLogger
    ctx                 context.Context
    coordinateTree      utils.CoordinateKdTree
    countryTable        map[string]int64
    regionTable         map[string]int64
    cityTable           map[string]int64
}

// NewAppnexusMapper create a new instance
func NewAppnexusMapper(cfg AppnexusMapperConf) IAppnexusMapper{
    instance := &appnexusMapper{
        log: cfg.Log,
        ctx: cfg.Ctx,
    }
    instance.initGeoTable(cfg.CountryFilename, cfg.RegionFilename, cfg.CityFilename)
    instance.initLatlonMapper(cfg.LatlonFilename)
    return instance
}

func (m *appnexusMapper) AppnexusCityIDInRange(radius float64, lat float64, lon float64) []int64 {
    contents := m.coordinateTree.inRadiusKNN(radius, lat, lon)

    if len(contents) == 0 {
        m.log.Infof("No city match!")
    }

    var ids []int64
    for _, content := range contents {
        cityContent := content.(appnexusCityContent)
        ids = append(ids, cityContent.appnexusCode)
    }

    m.log.Infof("Found %d targets for the location [%f, %f, %f]", len(ids), lat, lon, radius)
    return ids
}

func (m *appnexusMapper) AppnexusCityFromPolygon(points []utils.GeoPoint) []int64 {
    m.log.Infof("************** Inside appnexusCityFromPolygon ***************")
    grid := utils.GeoGrid{
        vertexPoints: points,
        // distance:     200, // Currently use approximate method: 1 degrees ~ 111 km
    }
    var appnexusIDs []int64
    appnexusMap := make(map[int64]bool)
    if !grid.build() {
        m.log.Errorf("Error while building grid")
        return appnexusIDs
    }
    for _, center := range grid.getCentralCell() {
        ids := m.AppnexusCityIDInRange(50, center.lat, center.lon)
        for i := 0; i < len(ids); i++ {
            if _, found := appnexusMap[ids[i]]; !found {
                appnexusMap[ids[i]] = true
                appnexusIDs = append(appnexusIDs, ids[i])
                m.log.Infof("[appnexusCityFromPolygon] appnexusCode = %d", ids[i])
            }
        }
    }
    m.log.Infof("[appnexusCityFromPolygon] There are %d appnexusCode", len(appnexusIDs))
    return appnexusIDs
}

func (m *appnexusMapper) AppnexusCountryFromCode(code string) int64 {
    if id, found := m.countryTable[code]; found {
        return id
    }
    return -1
}

func (m *appnexusMapper) AppnexusRegionFromCode(code string) int64 {
    if id, found := m.regionTable[code]; found {
        return id
    }
    return -1
}

func (m *appnexusMapper) AppnexusCityFromCode(code string) int64 {
    if id, found := m.cityTable[code]; found {
        return id
    }
    return -1
}

func (m *appnexusMapper) initLatlonMapper(filename string) {
    file, err := os.Open(filename)
    if err != nil {
        m.log.Errorf("could not load file: %s, error: %s", filename, err.Error())
        return
    }
    defer file.Close()

    reader := csv.NewReader(file)
    reader.FieldsPerRecord = 6
    data, err := reader.ReadAll()
    if err != nil {
        m.log.Errorf("error loading table data, err: %s", err.Error())
        return
    }

    m.coordinateTree = utils.CoordinateKdTree{}
    for _, row := range data {
        if len(row) != 6 {
            m.log.Errorf("malformed line in adx latlon table: ", row)
            continue
        }

        lat, err1 := strconv.ParseFloat(row[4], 64)
        lon, err2 := strconv.ParseFloat(row[5], 64)
        id, err3 := strconv.ParseInt(row[0], 10, 64)

        if err1 != nil || err2 != nil || err3 != nil {
            m.log.Errorf("cannot convert parameters for row: ", row)
            continue
        } else {
            content := types.AppnexusCityContent {
                row[1], row[2], row[3], int64(id),
            }
            m.coordinateTree.addNewPoint(content, lat, lon)
        }
    }
}

func (m *appnexusMapper) initGeoTable(countryFilename string, regionFilename string, cityFilename string) {
    m.countryTable = make(map[string]int64)
    m.regionTable = make(map[string]int64)
    m.cityTable = make(map[string]int64)

    m.initTable(2, countryFilename, func(row []string) {
        if len(row) != 2 {
            m.log.Errorf("malformed line in table: ", row)
            return
        }

        if id, err := strconv.ParseInt(row[0], 10, 64); err != nil {
            m.log.Errorf("cannot convert parameters for row: ", row)
        } else {
            m.countryTable[row[1]] = id
        }
    })

    m.initTable(3, regionFilename, func(row []string) {
        if len(row) != 3 {
            m.log.Errorf("malformed line in table: ", row)
            return
        }

        if id, err := strconv.ParseInt(row[0], 10, 64); err != nil {
            m.log.Errorf("cannot convert parameters for row: ", row)
        } else {
            key := row[1] + ":" + row[2]
            m.regionTable[key] = id
        }
    })

    m.initTable(4, cityFilename, func(row []string) {
        if len(row) != 4 {
            m.log.Errorf("malformed line in table: ", row)
            return
        }

        if id, err := strconv.ParseInt(row[3], 10, 64); err != nil {
            m.log.Errorf("cannot convert parameters for row: ", row)
        } else {
            m.cityTable[row[2]] = id
        }
    })
}

func (m *appnexusMapper) initTable(fields int, filename string, f func([]string)) {
    file, err := os.Open(filename)
    if err != nil {
        m.log.Errorf("could not load file: %s, error: %s", filename, err.Error())
        return
    }
    defer file.Close()

    reader := csv.NewReader(file)
    reader.FieldsPerRecord = fields
    data, err := reader.ReadAll()
    if err != nil {
        m.log.Errorf("error loading table data, err: %s", err.Error())
        return
    }

    for _, row := range data {
        f(row)
    }

    m.log.Infof("appnexus mapper init table %s successfully with %d records", filename, len(data))
}
