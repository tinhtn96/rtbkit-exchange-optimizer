package client

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"bitbucket.org/tinhtn1508/rtbkit-exchange-optimizer/pkg/types"
	"bitbucket.org/tinhtn1508/rtbkit-go-library/utils"
	"go.uber.org/zap"
)

// IAppneusClient interface
type IAppnexusClient interface {
    Init() bool
    ApplyProfile(string, []int64, []int64, []int64) int64
    FindProfileWithName(string) int64
    CreateProfileWithConfig(string, []int64, []int64, []int64) int64
    CreateProfile(string) int64
    ModifyProfileWithConfig(string, int64, []int64, []int64, []int64) int64
}

// AppnexusClientConf configuration
type AppnexusClientConf struct {
    Log                     *zap.SugaredLogger
    Ctx                     context.Context
    AppnexusAuthorizedAcc   types.AppnexusAuthorizedAccount
}

type appnexusClient struct {
    sync.RWMutex
    log                     *zap.SugaredLogger
    ctx                     context.Context
    accountAuthorization    types.AppnexusAuthorizedData
    bidderID                int64
}

// NewAppnexusClient for creating new instance
func NewAppnexusClient(cfg AppnexusClientConf) IAppnexusClient {
    return &appnexusClient{
        log:    cfg.Log,
        ctx:    cfg.Ctx,
        accountAuthorization: types.AppnexusAuthorizedData{
            Account: cfg.AppnexusAuthorizedAcc,
        },
    }
}

func (c *appnexusClient) Init() bool {
    if (!c.renewAuthorizationToken()) {
        c.log.Error("failed to authorize to appnexus!")
        return false
    }

    bidderId := c.getBidderID()
    if bidderId < 0 {
        c.log.Error("failed to get bidderID")
        return false
    }
    c.bidderID = bidderId
    c.log.Infof("Init appnexus API with bidder ID = %d", bidderId)

    c.startAuthorizationLoop()
    return true
}

func (c *appnexusClient) ApplyProfile(profileName string, countryId []int64, regionId []int64, cityId []int64) int64 {
    id := c.FindProfileWithName(profileName)
    if id < 0 {
        id = c.CreateProfileWithConfig(profileName, countryId, regionId, cityId)
    } else {
        id = c.ModifyProfileWithConfig(profileName, id, countryId, regionId, cityId)
    }
    return id
}

func (c *appnexusClient) FindProfileWithName(profileName string) int64 {
    profileMap := c.getBidderPretargetingProfiles()
    if id, found := profileMap[profileName]; found {
        c.log.Infof("found profile %s with ID = %d", profileName, id)
        return id
    }
    return -1
}

func (c *appnexusClient) CreateProfileWithConfig(profileName string, countryId []int64, regionId []int64, cityId []int64) int64 {
    profile := c.createProfile(profileName, countryId, regionId, cityId)

    jsonStr, err := json.Marshal(profile)
    if err != nil {
        c.log.Error(err)
        return -1
    }

    url := fmt.Sprintf(types.AppnexusAddProfileUrl, c.bidderID)
    resp, err := c.postJsonWithCredential(url, jsonStr)
    m := types.BidderAddPretargetingProfileResponse{}
    if err := json.Unmarshal(resp, &m); err != nil {
        c.log.Error("failed parsing json payload!")
        return -1
    }

    if m.ResponseBody.Status != "OK" {
        c.log.Errorf("cannot create new pretargeting filter, status = %s", m.ResponseBody.Status)
        return -1
    }

    countries := len(m.ResponseBody.Profile.CountryTargets)
    regions := len(m.ResponseBody.Profile.RegionTargets)
    cities := len(m.ResponseBody.Profile.CityTargets)
    c.log.Infof("Create new profile (%s) successfully, ID = %d (with %d countries, %d regions, %d cities)", profileName, m.ResponseBody.Profile.Id, countries, regions, cities)
    return m.ResponseBody.Profile.Id
}

func (c *appnexusClient) CreateProfile(profileName string) int64 {
    tmp := make([]int64, 0)
    return c.CreateProfileWithConfig(profileName, tmp, tmp, tmp)
}

func (c *appnexusClient) ModifyProfileWithConfig(profileName string, profileId int64, countryId []int64, regionId []int64, cityId []int64) int64 {
    profile := c.createProfile(profileName, countryId, regionId, cityId)
    c.log.Infof("Modify profile %d with %d countries, %d regions, %d cities", profileId, len(countryId), len(regionId), len(cityId))

    jsonStr, err := json.Marshal(profile)
    if err != nil {
        c.log.Error(err)
        return -1
    }

    url := fmt.Sprintf(types.AppnexusModifyProfileUrl, c.bidderID, profileId)
    resp, err := c.putJsonWithCredential(url, jsonStr)
    m := types.BidderAddPretargetingProfileResponse{}
    if err := json.Unmarshal(resp, &m); err != nil {
        c.log.Error("failed parsing json payload!")
        c.log.Errorf("payload: %s", string(resp))
        return -1
    }

    if m.ResponseBody.Status != "OK" {
        c.log.Errorf("cannot update pretargeting filter, status = %s", m.ResponseBody.Status)
        return -1
    }

    countries := len(m.ResponseBody.Profile.CountryTargets)
    regions := len(m.ResponseBody.Profile.RegionTargets)
    cities := len(m.ResponseBody.Profile.CityTargets)
    c.log.Infof("Modify profile (%s) successfully, ID = %d (with %d countries, %d regions, %d cities)", profileName, m.ResponseBody.Profile.Id, countries, regions, cities)
    return m.ResponseBody.Profile.Id
}


func (c *appnexusClient) renewAuthorizationToken() bool {
    if len(c.accountAuthorization.Account.Username) == 0 || len(c.accountAuthorization.Account.Password) == 0 {
        c.log.Error("account haven't been set!")
        return false
    }

    jsonStr, err := json.Marshal(c.accountAuthorization)
    if err != nil {
        c.log.Error(err)
        return false
    }

    payload, err := utils.PostJSON(utils.HTTPClient5s(), types.AppnexusAuthorizeAccountUrl, jsonStr)
    if err != nil {
        c.log.Error(err)
        return false
    }

    m := types.AppnexusAuthorizedResponse{}
    if err := json.Unmarshal(payload, &m); err != nil {
        c.log.Error("failed parsing json payload!")
        return false
    }

    if m.ResponseBody.Status != "OK" {
        c.log.Errorf("cannot renew token, status = %s", m.ResponseBody.Status)
        return false
    }

    c.log.Infof("appnexus apis renew with token: %s", m.ResponseBody.Token)
    c.Lock()
    defer c.Unlock()
    c.accountAuthorization.AuthorizedToken = m.ResponseBody.Token
    return true
}

func (c *appnexusClient) getBidderID() int64 {
    response, err := c.getWithCredential(types.AppnexusGetBidderUrl)
    if err != nil {
        c.log.Error("Get Bidder ID failed while calling bidder API!")
        return -1
    }

    bidder := types.BidderProfileResponse{}
    if err := json.Unmarshal(response, &bidder); err != nil {
        c.log.Error("Get Bidder ID failed while parsing json payload!")
        return -1
    }

    if bidder.ResponseBody.Status != "OK" {
        c.log.Errorf("API return NG, status = %s", bidder.ResponseBody.Status)
        return -1
    }

    return bidder.ResponseBody.BidderProfile.Id
}

func (c *appnexusClient) getWithCredential(url string) ([]byte, error) {
    c.RLock()
    defer c.RUnlock()
    request, _ := http.NewRequest("GET", url, nil)
    request.Header.Add("Content-Type", "application/json")
    request.Header.Add("Authorization", c.accountAuthorization.AuthorizedToken)

    client := utils.HTTPClient5s()

    resp, errs := client.Do(request)
    body := []byte{}

    if errs == nil && resp != nil && resp.Body != nil {
        body, errs = ioutil.ReadAll(resp.Body)
        resp.Body.Close()
    }
    return body, errs
}


func (c *appnexusClient) startAuthorizationLoop() {
    authorizeLoop := func(c *appnexusClient) {
        if c == nil {
            c.log.Error("passing nil api object to function!")
            return
        }
        ticker := time.NewTicker(time.Minute * 60)
        for {
            select {
                case <- ticker.C:
                    c.renewAuthorizationToken()
            }
        }
    }

    go authorizeLoop(c)
}

func (c *appnexusClient) getBidderPretargetingProfiles() map[string]int64 {
    mp := make(map[string]int64)
    url := fmt.Sprintf(types.AppnexusGetProfileUrl, c.bidderID)
    response, err := c.getWithCredential(url)
    if err != nil {
        c.log.Error("Get profiles failed while calling profiles API!")
        return mp
    }

    profiles := types.BidderPreTargetingProfileResponse{}
    if err := json.Unmarshal(response, &profiles); err != nil {
        c.log.Error("Get profiles failed while parsing json payload!")
        return mp
    }

    if profiles.ResponseBody.Status != "OK" {
        c.log.Errorf("API return NG, status = %s", profiles.ResponseBody.Status)
        return mp
    }

    for _, item := range profiles.ResponseBody.Profiles {
        mp[item.Description] = item.Id
    }
    return mp
}

func (c *appnexusClient) createProfile(profileName string, countryId []int64, regionId []int64, cityId []int64) types.BidderPretargetingProfile {
    countryAction := "exclude"
    if len(countryId) > 0 {
        countryAction = "include"
    }

    regionAction := "exclude"
    if len(regionId) > 0 {
        regionAction = "include"
    }

    cityAction := "exclude"
    if len(cityId) > 0 {
        cityAction = "include"
    }

    profile := types.BidderPretargetingProfile{}
    profile.Profile.Description = profileName
    profile.Profile.CountryAction = countryAction
    profile.Profile.RegionAction = regionAction
    profile.Profile.CityAction = cityAction
    profile.Profile.CountryTargets = c.createConfigList(countryId)
    profile.Profile.RegionTargets = c.createConfigList(regionId)
    profile.Profile.CityTargets = c.createConfigList(cityId)
    return profile
}

func (c *appnexusClient) createConfigList(ids []int64) []types.GeoTargetingConfig {
    lst := make([]types.GeoTargetingConfig, 0)
    for _, id := range ids {
        config := types.GeoTargetingConfig{}
        config.Id = id
        lst = append(lst, config)
    }
    return lst
}

func (c *appnexusClient) postJsonWithCredential(url string, payload []byte) ([]byte, error) {
    return c.jsonWithCredential("POST", url, payload)
}

func (c *appnexusClient) putJsonWithCredential(url string, payload []byte) ([]byte, error) {
    return c.jsonWithCredential("PUT", url, payload)
}

func (c *appnexusClient) jsonWithCredential(action string, url string, payload []byte) ([]byte, error) {
    c.RLock()
    defer c.RUnlock()
    contentReader := bytes.NewReader(payload)
    request, _ := http.NewRequest(action, url, contentReader)
    request.Header.Add("Content-Type", "application/json")
    request.Header.Add("Authorization", c.accountAuthorization.AuthorizedToken)

    client := utils.HTTPClient5s()

    resp, errs := client.Do(request)
    body := []byte{}

    if errs == nil && resp != nil && resp.Body != nil {
        body, errs = ioutil.ReadAll(resp.Body)
        resp.Body.Close()
    }
    return body, errs
}
